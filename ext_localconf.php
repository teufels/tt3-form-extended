<?php
defined('TYPO3') or die();

call_user_func(function() {

    /**
     * Override LoggerFinisher with slightly different one (that extend 'file' by path,url)
     * Override LoggerFinisher add Multiple-File Upload Support (provided by form_extended)
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\Pagemachine\Formlog\Domain\Form\Finishers\LoggerFinisher::class] = [
        'className' => \Teufels\Tt3FormExtended\Domain\Finishers\LoggerFinisher::class
    ];

    /**
     * Override EmailFinisher add additional receiver from teufels career > JobOffer to BCC for Finisher MailToReceiver
     * Override EmailFinisher add Multiple-File Upload Support (provided by form_extended)
     * Override EmailFinisher of form_mailtext & form_extended (fixes error message=null on form_mailtext)
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\Kitzberger\FormMailtext\Domain\Finishers\EmailFinisher::class] = [
        'className' => \Teufels\Tt3FormExtended\Domain\Finishers\EmailFinisher::class
    ];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\WapplerSystems\FormExtended\Domain\Finishers\EmailFinisher::class] = [
        'className' => \Teufels\Tt3FormExtended\Domain\Finishers\EmailFinisher::class
    ];

    /**
     * Fix error Exception while property mapping at property path "": Property "0" was not found in target object of type "TYPO3\CMS\Extbase\Domain\Model\FileReference".
     * on back with multiple files & forms with steps
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Form\ViewHelpers\Form\UploadedResourceViewHelper::class] = [
        'className' => \Teufels\Tt3FormExtended\ViewHelpers\UploadedResourcesViewHelper::class
    ];
    
});