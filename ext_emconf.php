<?php

$EM_CONF[$_EXTKEY] = [
    'title' => '[teufels] Form Extended',
    'description' => 'Override/Extend form & collection from based extensions',
    'category' => 'misc',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'state' => 'stable',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'form' => '11.5.0-12.4.99',
            'typo3-formlog' => '>=2.2.1',
            'form_extended' => '>=12.1.6',
            'form-mailtext' => '>=2.1.4',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
