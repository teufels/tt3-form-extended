<?php
declare(strict_types = 1);
namespace Teufels\Tt3FormExtended\Domain\Finishers;

use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Mail\MailerInterface;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Form\Domain\Finishers\Exception\FinisherException;
use TYPO3\CMS\Form\Domain\Model\FormElements\FileUpload;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime;
use TYPO3\CMS\Form\Service\TranslationService;

class EmailFinisher extends \Kitzberger\FormMailtext\Domain\Finishers\EmailFinisher
{

    /**
     * Executes this finisher
     * @see AbstractFinisher::execute()
     *
     * @throws FinisherException
     */
    protected function executeInternal()
    {
        $languageBackup = null;
        // Flexform overrides write strings instead of integers so
        // we need to cast the string '0' to false.
        if (
            isset($this->options['addHtmlPart'])
            && $this->options['addHtmlPart'] === '0'
        ) {
            $this->options['addHtmlPart'] = false;
        }

        $subject = (string)$this->parseOption('subject');
        $recipients = $this->getRecipients('recipients');
        $senderAddress = $this->parseOption('senderAddress');
        $senderAddress = is_string($senderAddress) ? $senderAddress : '';
        $senderName = $this->parseOption('senderName');
        $senderName = is_string($senderName) ? $senderName : '';
        $replyToRecipients = $this->getRecipients('replyToRecipients');
        $carbonCopyRecipients = $this->getRecipients('carbonCopyRecipients');
        $blindCarbonCopyRecipients = $this->getRecipients('blindCarbonCopyRecipients');
        $addHtmlPart = $this->parseOption('addHtmlPart') ? true : false;
        $attachUploads = $this->parseOption('attachUploads');
        $title = (string)$this->parseOption('title') ?: $subject;

        if ($subject === '') {
            throw new FinisherException('The option "subject" must be set for the EmailFinisher.', 1327060320);
        }
        if (empty($recipients)) {
            throw new FinisherException('The option "recipients" must be set for the EmailFinisher.', 1327060200);
        }
        if (empty($senderAddress)) {
            throw new FinisherException('The option "senderAddress" must be set for the EmailFinisher.', 1327060210);
        }

        $formRuntime = $this->finisherContext->getFormRuntime();

        $translationService = GeneralUtility::makeInstance(TranslationService::class);
        if (is_string($this->options['translation']['language'] ?? null) && $this->options['translation']['language'] !== '') {
            $languageBackup = $translationService->getLanguage();
            $translationService->setLanguage($this->options['translation']['language']);
        }

        $mail = $this
            ->initializeFluidEmail($formRuntime)
            ->from(new Address($senderAddress, $senderName))
            ->to(...$recipients)
            ->subject($subject)
            ->format($addHtmlPart ? FluidEmail::FORMAT_BOTH : FluidEmail::FORMAT_PLAIN)
            ->assign('title', $title);

        if (!empty($replyToRecipients)) {
            $mail->replyTo(...$replyToRecipients);
        }

        if (!empty($carbonCopyRecipients)) {
            $mail->cc(...$carbonCopyRecipients);
        }

        /**
         * add additional receiver from teufels career > JobOffer to BCC for Finisher MailToReceiver
         */
        $formValues = $formRuntime->getFormState()->getFormValues();
        if(!empty($formValues['receiver-additional']) && $formRuntime->getCurrentFinisher()->finisherIdentifier == 'EmailToReceiver') {
            $receiverAdditional = preg_split('/;|,/', trim($formValues['receiver-additional']));
            foreach ($receiverAdditional as $address) {
                if (!GeneralUtility::validEmail($address)) {
                    // Drop entries without valid address
                    continue;
                }
                $blindCarbonCopyRecipients[] = new Address($address);
            }
        }
        if (!empty($blindCarbonCopyRecipients)) {
            $mail->bcc(...$blindCarbonCopyRecipients);
        }

        if (!empty($languageBackup)) {
            $translationService->setLanguage($languageBackup);
        }

        if ($attachUploads) {
            foreach ($formRuntime->getFormDefinition()->getRenderablesRecursively() as $element) {
                if (!$element instanceof FileUpload) {
                    continue;
                }
                $file = $formRuntime[$element->getIdentifier()];
                if ($file) {
                    /**
                     * add Multiple-File Upload Support (provided by form_extended)
                     */
                    if (is_array($file)) {
                        $files = $file;
                        foreach ($files as &$file) {
                            if ($file instanceof FileReference) {
                                $file = $file->getOriginalResource();
                            }
                            if (!empty($file)) {
                                $mail->attach($file->getContents(), $file->getName(), $file->getMimeType());
                            }
                        }
                    } else {
                        if ($file instanceof FileReference) {
                            $file = $file->getOriginalResource();
                        }
                        if (!empty($file)) {
                            $mail->attach($file->getContents(), $file->getName(), $file->getMimeType());
                        }
                    }
                }
            }
        }

        // TODO: DI should be used to inject the MailerInterface
        GeneralUtility::makeInstance(MailerInterface::class)->send($mail);
    }

}
