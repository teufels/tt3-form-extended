[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--form--extended-orange.svg)](https://bitbucket.org/teufels/tt3-form-extended/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3_form__extended-red.svg)](https://bitbucket.org/teufels/tt3-form-extended/src/main/)
![version](https://img.shields.io/badge/version-1.0.*-yellow.svg?style=flat-square)

[ ṯeufels ] Form Extended
==========
Override/Extend form & collection from based extensions

#### This version supports TYPO3
![CUSTOMER](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

#### Composer support
`composer req teufels/tt3-form-extended`

***

## Requirements
- `"typo3/cms-form": "11.5 || 12.4"`
- `"pagemachine/typo3-formlog": "^2.2"`
- `"kitzberger/form-mailtext" : "^2.1"`
- `"wapplersystems/form_extended": "^12.1"`
*** 

## Notice
- Overrides "EmailFinisher" & "LoggerFinisher" for Multiple-File Upload Support (provided by form_extended)
- Overrides "EmailFinisher" to add additional receiver from teufels career > JobOffer to BCC for Finisher MailToReceiver

## ToDo
- Fix error Exception while property mapping at property path "": Property "0" was not found in target object of type "TYPO3\CMS\Extbase\Domain\Model\FileReference"<br> 
  on back with multiple files & forms with steps<br>
  is actual disabled seems not to be needed anymore.<br>
  ToDo: check & remove if not needed anymore

*** 

## Documentation
- https://github.com/WapplerSystems/form_extended

***

## Changelog
### [1.0.0] - 2024-08-20
- [MAJOR] Tested & set version to 1.0.0 stable
### [0.1.0] - 2024-06-10
- [ADDED] readd requirement of form_extended
### [0.0.1] - 2023-09-22
- [BREAKING] intial from [hive_ovr_form](https://bitbucket.org/teufels/hive_ovr_form/src/) `release/v12` but removed requirement of form_extended (=> remove support multifile-upload) not TYPO3 v12 ready yet